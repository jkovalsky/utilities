# Utilities

The *Utilities API* are a group of useful utilities that PekinSOFT Systems has
been working on for a little over a decade. These utilities include string
manipulation, password manipulation, file manipulation, color testing, date
conversions and calculations, screen positioning, error printing, etc.

Each of the individual utility classes focus on a single element. For example,
the `PasswordUtils` class only provides methods useful for password manipulation,
such as hashing and validation, as well as password rules enforcement. Likewise,
the `ScreenUtils` class only provides methods to assist in window placement on
the user's screen(s).

We believe that these utilities can come in handy for various projects, so we 
have separated them out of our conglomerate API, as we are refactoring a lot of
our individual APIs.

## License

This library is released under the terms of the GNU Lesser General Public License
version 3.