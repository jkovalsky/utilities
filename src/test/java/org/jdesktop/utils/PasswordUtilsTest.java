/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Utilities
 *  Class      :   PasswordUtilsTest.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 20, 2023
 *  Modified   :   Jan 20, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 20, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import java.util.Optional;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sean Carrick
 */
public class PasswordUtilsTest {
    
    public PasswordUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isPasswordValid method, of class PasswordUtils.
     */
    @Test
    public void testIsPasswordValid() {
        System.out.println("isPasswordValid");
        String pword = "592*-:1J0hnD0u6h";
        char[] password = pword.toCharArray();
        int minLength = 10;
        int minUppercase = 2;
        int minNumbers = 3;
        int minSymbols = 3;
        assertTrue(PasswordUtils.isPasswordValid(password, minLength, minUppercase, minNumbers, minSymbols));
    }
    /**
     * Test of isPasswordValid method, of class PasswordUtils.
     */
    @Test
    public void testIsPasswordNotValid() {
        System.out.println("isPasswordValid");
        String pword = "592*-1J0hnd0u6h";
        char[] password = pword.toCharArray();
        int minLength = 10;
        int minUppercase = 2;
        int minNumbers = 3;
        int minSymbols = 3;
        assertFalse(PasswordUtils.isPasswordValid(password, minLength, minUppercase, minNumbers, minSymbols));
    }
    
}
