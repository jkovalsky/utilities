/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   Utilities
 *  Class      :   StringUtilsTest.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 20, 2023
 *  Modified   :   Jan 20, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 20, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Sean Carrick
 */
public class StringUtilsTest {
    
    public StringUtilsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of abbreviate method, of class StringUtils.
     */
    @Test
    public void testAbbreviate() {
        System.out.println("abbreviate");
        String source = "Abbreviation";
        int width = 7;
        String expResult = "Abbr...";
        String result = StringUtils.abbreviate(source, width);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteWhitespace method, of class StringUtils.
     */
    @Test
    public void testDeleteWhitespace() {
        System.out.println("deleteWhitespace");
        String source = "This is a test string.";
        String expResult = "Thisisateststring.";
        String result = StringUtils.deleteWhitespace(source);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeStart method, of class StringUtils.
     */
    @Test
    public void testRemoveStart() {
        System.out.println("removeStart");
        String source = "Testing one, two, three";
        String remove = "Testing ";
        String expResult = "one, two, three";
        String result = StringUtils.removeStart(source, remove);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeStartIgnoreCase method, of class StringUtils.
     */
    @Test
    public void testRemoveStartIgnoreCase() {
        System.out.println("removeStartIgnoreCase");
        String source = "Testing one, two, three";
        String remove = "testing ";
        String expResult = "one, two, three";
        String result = StringUtils.removeStartIgnoreCase(source, remove);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeEnd method, of class StringUtils.
     */
    @Test
    public void testRemoveEnd() {
        System.out.println("removeEnd");
        String source = "Testing one, two, three";
        String remove = "one, two, three";
        String expResult = "Testing ";
        String result = StringUtils.removeEnd(source, remove);
        assertEquals(expResult, result);
    }

    /**
     * Test of padLeft method, of class StringUtils.
     */
    @Test
    public void testPadLeft() {
        System.out.println("padLeft");
        String toPad = "Once";
        int fieldWidth = 10;
        String expResult = "Once      ";
        String result = StringUtils.padLeft(toPad, fieldWidth);
        assertEquals(expResult, result);
    }

    /**
     * Test of padRight method, of class StringUtils.
     */
    @Test
    public void testPadRight() {
        System.out.println("padRight");
        String toPad = "Once";
        int fieldWidth = 10;
        String expResult = "      Once";
        String result = StringUtils.padRight(toPad, fieldWidth);
        assertEquals(expResult, result);
    }

    /**
     * Test of repeat method, of class StringUtils.
     */
    @Test
    public void testRepeat() {
        System.out.println("repeat");
        String toRepeat = "*";
        int times = 10;
        String expResult = "**********";
        String result = StringUtils.repeat(toRepeat, times);
        assertEquals(expResult, result);
    }

    /**
     * Test of wrap method, of class StringUtils.
     */
    @Test
    public void testWrap() {
        System.out.println("wrap");
        String source = "It was the best of times, it was the worst of times, it "
                + "was the age of wisdom, it was the age of foolishness, it was "
                + "the epoch of belief, it was the epoch of incredulity, it was the "
                + "season of Darkness, it was the spring of hope, it was the winter "
                + "of despair, we had everything before us, we had nothing before "
                + "us, we were all going direct to Heaven, we were all going direct "
                + "the other way - in short, the period was so far like the present "
                + "period that some of its noisiest authorities insisted on its "
                + "being received for good or for evil, in the superlative degree "
                + "of comparison only.";
        int width = 80;
        String expResult 
                = "It was the best of times, it was the worst of times, it was the age of wisdom,\n" +
"it was the age of foolishness, it was the epoch of belief, it was the epoch of\n" +
"incredulity, it was the season of Darkness, it was the spring of hope, it was\n" +
"the winter of despair, we had everything before us, we had nothing before us,\n" +
"we were all going direct to Heaven, we were all going direct the other way - in\n" +
"short, the period was so far like the present period that some of its noisiest\n" +
"authorities insisted on its being received for good or for evil, in the\n" +
"superlative degree of comparison only.";
        System.out.println("expResult:");
        System.out.println(expResult);
        String result = StringUtils.wrap(source, width);
        System.out.println("result:");
        System.out.println(result);
        assertTrue(expResult.equals(result));
    }

    /**
     * Test of wrapLogMessage method, of class StringUtils.
     */
    @Test
    public void testWrapLogMessage() {
        System.out.println("wrapLogMessage");
        String source = "Wrapped:message";
        int width = 9;
        String expResult = "Wrapped\nmessage";
        String result = StringUtils.wrapLogMessage(source, width);
        assertEquals(expResult, result);
    }

    /**
     * Test of insertTabLeader method, of class StringUtils.
     */
    @Test
    public void testInsertTabLeader() {
        System.out.println("insertTabLeader");
        String leftWord = "Left";
        String rightWord = "Right";
        int rightMargin = 15;
        char leader = '.';
        String expResult = "Left......Right";
        String result = StringUtils.insertTabLeader(leftWord, rightWord, rightMargin, leader);
        assertEquals(expResult, result);
    }

    /**
     * Test of centerString method, of class StringUtils.
     */
    @Test
    public void testCenterString() {
        System.out.println("centerString");
        String toCenter = "Centered";
        int withinBounds = 16;
        String expResult = "    Centered";
        String result = StringUtils.centerString(toCenter, withinBounds);
        assertEquals(expResult, result);
    }
    
}
