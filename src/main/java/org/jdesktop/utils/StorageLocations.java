/*
 * Copyright (C) 2023 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   LookupAPI
 *  Class      :   StorageLocations.java
 *  Author     :   Sean Carrick
 *  Created    :   Jan 19, 2023
 *  Modified   :   Jan 19, 2023
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Jan 19, 2023  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.utils;

/**
 * {@code StorageLocations} enumerates constants for common file storage locations
 * needed by applications. These constants are used for determining directories
 * in which files may be read or written. The locations will typically be 
 * determined by the operating system (OS) on which the application is running.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public enum StorageLocations {
    /** Configuration directory, where session state settings are stored. */
    CONFIG_DIR,
    /** Data directory, where user data is typically stored. */
    DATA_DIR,
    /** Error directory, where critical error logs are typically stored. */
    ERROR_DIR,
    /** File system, for getting/putting a file outside of the application's
     * directory.
     */
    FILE_SYSTEM,
    /** Log directory, where common log files are typically stored. */
    LOG_DIR,
    /** Options directory, where uncommon options may be stored. */
    OPTIONS_DIR,
    /** Plugins directory, where application plugins will be installed. */
    PLUGINS_DIR,
    /** System directory, a common location for application "system" settings
     * and configurations to be stored.
     */
    SYSTEM_DIR,
    /** Variants directory, where variable information that falls into no other
     * category may be stored. The definition of this type of data is very much
     * application-specific.
     */
    VARIANTS_DIR
}
