/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   PekinSOFT_Utils
 *  Class      :   Utilities.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 11, 2022
 *  Modified   :   Dec 11, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 11, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.utils;

import java.io.File;

/**
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class Utilities {
    
    private Utilities () { /* No instantiation allowed. */ }
    
    /**
     * Gets the name of the program's JAr file.
     * 
     * @return the program's JAr file name
     */
    public static String getJarName() {
        return new File(Utilities.class.getProtectionDomain()
                .getCodeSource()
                .getLocation()
                .getPath())
                .getName();
    }
    
    /**
     * Tests to see if the project is currently executing from within a JAr file
     * or from a folder structure. This is useful for testing if the program is
     * running inside or outside of an IDE.
     * 
     * @return {@code true} if running inside a JAr file; {@code false} otherwise
     */
    private static boolean runningFromJar() {
        String jarName = getJarName();
        return jarName.endsWith(".jar");
    }
    
    /**
     * Retrieves the currently executing program's program directory. This should
     * be the directory in which the program was executed, which could also be
     * considered the program's installation path.
     * 
     * @return the directory from which the program is running
     */
    public static String getProgramDirectory() {
        if (runningFromJar()) {
            return getCurrentJarDirectory();
        } else {
            return getCurrentProjectDirectory();
        }
    }
    
    /**
     * Retrieves the current project's directory.
     * 
     * @return the project's directory
     */
    private static String getCurrentProjectDirectory() {
        return new File("").getAbsolutePath();
    }
    
    /**
     * Retrieves the JAr file's current directory location.
     * 
     * @return the directory in which the JAr file is located
     */
    private static String getCurrentJarDirectory() {
        return new File(getJarName()).getParent();
    }

}
