/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   PekinSOFT_Utils
 *  Class      :   TerminalErrorPrinter.java
 *  Author     :   Sean Carrick
 *  Created    :   Dec 11, 2022
 *  Modified   :   Dec 11, 2022
 *  
 *  Purpose: See class JavaDoc for explanation
 *  
 *  Revision History:
 *  
 *  WHEN          BY                   REASON
 *  ------------  -------------------  -----------------------------------------
 *  Dec 11, 2022  Sean Carrick         Initial creation.
 * *****************************************************************************
 */

package org.jdesktop.utils;

/**
 * A simple static class to send error messages to the console. This is <em>not</em>
 * a comprehensive error handling class.
 *
 * @author Sean Carrick &lt;sean at pekinsoft dot com&gt;
 * 
 * @version 1.0
 * @since 1.0
 */
public class TerminalErrorPrinter {
    
    private TerminalErrorPrinter () { /* No instantiation allowed. */ }
    
    /**
     * Prints an error and its associated message to both {@link java.lang.System#out
     * System.out} and {@link java.lang.System#err System.err}, as needed by the
     * current process in the method.
     * 
     * @param ex the exception that was caught
     * @param msg the accompanying message, may be null
     */
    public static void print(Throwable ex, String msg) {
        StringBuilder sb = new StringBuilder();
        sb.append("The following error was encountered in the application:\n\n");
        sb.append(StringUtils.repeat("-", 80)).append("\n");
        sb.append(msg).append("\n");
        sb.append(StringUtils.repeat("-", 80)).append("\n");
        sb.append("Localized Message: ").append(ex.getLocalizedMessage());
        sb.append("\n            Cause: ").append(ex.getCause()).append("\n");
        sb.append(StringUtils.repeat("-", 80)).append("\n");
        sb.append("Stacktrace").append("\n");
        sb.append(StringUtils.repeat("-", 80)).append("\n");
        Throwable[] throwables = ex.getSuppressed();
        StackTraceElement[] elements = ex.getStackTrace();
        
        for (StackTraceElement e : elements) {
            sb.append(e.toString()).append("\n");
        }
        
        for (Throwable t : throwables) {
            sb.append(t.getCause().getClass().getSimpleName()).append("\n");
            sb.append(t.getLocalizedMessage()).append("\n");
            elements = t.getStackTrace();
            for (StackTraceElement e : elements) {
                sb.append("\t").append(e.toString()).append("\n");
            }
        }
        
        System.err.println(sb.toString());
    }

}
