/*
 * Copyright (C) 2022 PekinSOFT Systems
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 * *****************************************************************************
 *  Project    :   PekinSOFT_Utils
 *  Class      :   DateUtils.java
 *  Author     :   Sean Carrick
 *  Created    :   Mar 8, 2020 @ 1:43:17 PM
 *  Modified   :   Mar 8, 2020
 *
 *  Purpose:
 *
 *  Revision History:
 *
 *  WHEN          BY                  REASON
 *  ------------  ------------------- ------------------------------------------
 *  Mar 8, 2020  Sean Carrick        Initial creation.
 * *****************************************************************************
 */
package org.jdesktop.utils;

import java.lang.System.Logger;
import java.math.BigInteger;
import java.sql.Time;
import java.sql.Timestamp;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.util.Date;

/**
 * {@code DateUtils} provides static methods for various date calculations and
 * conversions.
 *
 * @author Sean Carrick &lt;PekinSOFT at outlook dot com&gt;
 * 
 * @version 0.1.0
 * @since 0.1.0
 */
public class DateUtils {
    
    public static enum DateTypes {
        /** Constant for retrieving a {@link java.util.Date java.util.Date}. */
        DATE,
        /** Constant for retrieving a {@link java.time.LocalDate LocalDate}. */
        LOCAL_DATE,
        /** Constant for retrieving a {@link java.time.LocalTime LocalTime}. */
        LOCAL_TIME,
        /** Constant for retrieving a {@link java.time.LocalDateTime LocalDateTime}. */
        LOCAL_DATE_TIME,
        /** Constant for retrieving a {@link java.sql.Date java.sql.Date}. */
        SQL_DATE,
        /** Constant for retrieving a {@link java.sql.Time Time}. */
        SQL_TIME,
        /** Constant for retrieving a {@link java.sql.Timestamp Timestamp}. */
        SQL_TIMESTAMP,
    }
    
    /**
     * The number of milliseconds in one (1) year (i.e., 365 days)
     */
    public static final long MILLIS_PER_YEAR_STD = 31540000000L;
    /**
     * The number of milliseconds in one (1) year, exact (i.e., 365.25 days).
     */
    public static final long MILLIS_PER_YEAR_EX = 3155760000L;
    /**
     * The Epoch year for the Java programming language, which is 1970, with the
     * actual epoch being January 1, 1970 00:00:00.000.
     */
    public static final long EPOCH_YEAR = 1970;
    
    private static final Logger logger;
    
    /* Initializes the Logger object from the System */
    static {
        logger = System.getLogger("PS-Utils-API");
    }
    
    /* Private scope so that this class cannot be instantiated. */
    private DateUtils () { /* uninstantiable */ }
    
    /**
     * Retrieves the current year. This method relies on the date and time of 
     * the system being set correctly, as it uses {@code System.currentTimeMillis}
     * to obtain the current date and time from the system. It then calculates
     * the current year by dividing the current date/time in milliseconds by the
     * number of milliseconds in a year ({@code MILLIS_PER_YEAR}), then adding
     * the epoch year to that dividend ({@code EPOCH_YEAR}).
     * 
     * @return the current year as a {@code long} value
     */
    public static long getCurrentYear() {
        return (System.currentTimeMillis() / MILLIS_PER_YEAR_STD) + EPOCH_YEAR;
    }
    
    /**
     * Calculates the difference in time between two given instances.
     * <p>
     * This method calculates the difference between the two dates and returns
     * the value as a double. <em>This method calculates down to the millisecond
     * level, so the difference in the two times is exact.</em>
     * 
     * @param start the start date/time of the calculation ({@code *-ago})
     * @param end the end date/time of the calculation ({@code -*})
     * @return the difference between the two times as a {@code long} value
     * @throws DateTimeParseException if either of the specified dates cannot be
     * parsed into valid dates
     */
    public static long calculateDifferenceInNanos(LocalDateTime start, 
            LocalDateTime end) {
        return getTimeSpent(Duration.between(start, end));
    }
    
    /**
     * Calculates the difference in time between two given instances.
     * <p>
     * This method calculates the difference between the two dates and returns
     * the value as a double. <em>This method calculates down to the nanosecond
     * level, so the difference in the two times is exact.</em>
     * 
     * @param start the start date/time of the calculation ({@code *-ago})
     * @param end the end date/time of the calculation ({@code -*})
     * @return the difference between the two times as a {@code double} value
     * @throws DateTimeParseException if either of the specified dates cannot be
     * parsed into valid dates
     */
    public static long calculateDifferenceInNanos(Time start, Time end) {
        return getTimeSpent(Duration.between(start.toLocalTime(), end.toLocalTime()));
    }
    
    /**
     * Takes the supplied {@link java.time.Duration Duration} and return the
     * amount of time contained in it broken down to total <em>nanoseconds</em>.
     * 
     * @param duration the {@code Duration} between two points in time
     * @return the total number of nanoseconds spent in that period
     * @throws ArithmeticException if an overflow situation occurs; this could
     * happen if the number of nanoseconds in the {@code Duration} exceed {@link
     * java.lang.Long#MAX_VALUE Long.MAX_VALUE}
     */
    private static long getTimeSpent(Duration duration) {
        return duration.toNanos();
    }
    
    /**
     * Calculates the difference in time between the two given instances, then
     * determining the number of seconds they make up. The parameters provide
     * the values as strings.
     * 
     * @param start the earlier time of the calculation ({@code * - start})
     * @param end the later time of the calculation ({@code end - *})
     * @return the difference in seconds between the two times as a {@code 
     * double} value
     * @throws DateTimeParseException if either of the specified dates cannot be
     * parsed into valid dates
     */
    public static double calculateDifferenceInSeconds(LocalDateTime start, 
            LocalDateTime end) {
        return calculateDifferenceInNanos(start, end) / 1_000_000_000;
    }
    
    
    /**
     * Calculates the difference in time between the two given instances, then
     * determining the number of minutes they make up. The parameters provide
     * the values as strings.
     * 
     * @param ago the earlier time of the calculation ({@code *-ago})
     * @param later the later time of the calculation ({@code -*})
     * @return the difference in minutes between the two times as a {@code 
     * double} value
     * @throws DateTimeParseException if either of the specified dates cannot be
     * parsed into valid dates
     */
    public static double calculateDifferenceInMinutes(LocalDateTime ago, 
            LocalDateTime later) {
        return calculateDifferenceInSeconds(ago, later) / 60;
    }
    
    /**
     * Calculates the difference in time between the two given instances, then
     * determining the number of hours they make up. The parameters provide
     * the values as strings.
     * 
     * @param ago the earlier time of the calculation ({@code *-ago})
     * @param later the later time of the calculation ({@code -*})
     * @return the difference in hours between the two times as a {@code double}
     * value
     * @throws DateTimeParseException if either of the specified dates cannot be
     * parsed into valid dates
     */
    public static double calculateDifferenceInHours(LocalDateTime ago, 
            LocalDateTime later) {
        return calculateDifferenceInMinutes(ago, later) / 60;
    }
    
    /**
     * Calculates the difference in time between the two given instances, then
     * determining the number of days they make up. The parameters provide
     * the values as strings.
     * 
     * @param ago the earlier time of the calculation ({@code *-ago})
     * @param later the later time of the calculation ({@code -*})
     * @return the difference in days between the two times as a {@code double} 
     * value
     * @throws DateTimeParseException if either of the specified dates cannot be
     * parsed into valid dates
     */
    public static double calculateDifferenceInDays(LocalDateTime ago, 
            LocalDateTime later) {
        return calculateDifferenceInHours(ago, later) / 24;
    }
    
    /* CONVERSION METHODS */
    
    /**
     * Converts the specified {@link java.util.Date Date} from the Java
     * Util package to a {@link java.time.LocalDate LocalDate} from the
     * Java Time package.
     * 
     * @param date2Convert the {@code java.util.Date} to convert to a  
     * {@code java.time.LocalDate} instance
     * @return the specified {@code Date} as a {@code LocalDate} instance
     */
    public static LocalDate convertDate2LocalDate(Date date2Convert) {
        return date2Convert.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
    
    /**
     * Converts the specified {@link java.time.LocalDate LocalDate} from 
     * the Java Time package to a {@link java.util.Date Date} from the
     * Java Util package.
     * 
     * @param date2Convert the {@code java.time.LocalDate} to convert to a 
     * {@code java.util.Date} instance
     * @return the specified {@code LocalDate} as a {@code Date} instance
     */
    public static Date convertLocalDate2Date(LocalDate date2Convert) {
        return Date.from(date2Convert.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }
    
    /**
     * Converts the specified {@link java.util.Date Date} from the Java
     * Util package to a {@link java.time.LocalTime LocalTime} from the
     * Java Time package.
     * 
     * @param date2Convert the {@code java.util.Date} to convert to a 
     * {@code java.time.LocalTime} instance
     * @return the specified {@code Date} as a {@code LocalTime} instance
     */
    public static LocalTime convertDate2LocalTime(Date date2Convert) {
        Instant time = date2Convert.toInstant();
        return LocalTime.ofInstant(time, ZoneId.systemDefault());
//        return LocalTime.ofInstant(time, ZoneId.systemDefault());
    }
    
    /**
     * Converts the specified {@link java.util.Date Date} from the Java
     * Util package to a {@link java.sql.Date Date} from the Java SQL
     * package.
     * 
     * @param date2Convert the {@code java.util.Date} to convert to a {@code java.sql.Date}
     * instance
     * @return the specified {@code java.util.Date} as a {@code java.sql.Date}
     * instance
     */
    public static java.sql.Date convertDate2SqlDate(Date date2Convert) {
        return java.sql.Date.valueOf(convertDate2LocalDate(date2Convert));
    }
    
    /**
     * Converts the specified {@link java.util.Date Date} from the Java
     * Utuil package to a {@link java.sql.Time Time} from the Java SQL
     * package.
     * 
     * @param date2Convert the {@code java.util.Date} to convert to a {@code java.sql.Time}
     * instance
     * @return the specified {@code java.util.Date} as a {@code java.sql.Time}
     * instance
     */
    public static java.sql.Time convertDate2SqlTime(Date date2Convert) {
        return Time.valueOf(convertDate2LocalTime(date2Convert));
    }
    
    /**
     * Converts the specified {@link java.util.Date Date} from the Java
     * Util package to a {@link java.sql.Timestamp TimeStamp} from the 
     * Java SQL package.
     * 
     * @param date2convert the {@code java.util.Date} to convert to a {@code java.sql.TimeStamp}
     * instance
     * @return the specified {@code java.util.Date} as a {@code java.sql.TimeStamp}
     * instance
     */
    public static java.sql.Timestamp convertDate2SqlTimestamp(Date date2convert) {
        return java.sql.Timestamp.valueOf(convertDate2LocalDateTime(date2convert));
    }
    
    /**
     * Converts the specified {@link java.time.LocalTime LocalTime} from 
     * the Java Time package to a {@link java.util.Date Date} from the
     * Java Util package.
     * 
     * @param time2Convert the {@code java.time.LocalTime} to convert to a 
     * {@code java.util.Date} instance
     * @return the specified {@code LocalTime} as a {@code Date} instance
     */
    public static Date convertLocalTime2Date(LocalTime time2Convert) {
        Instant now = time2Convert.atDate(LocalDate.now())
                .atZone(ZoneId.systemDefault()).toInstant();
        BigInteger millis = BigInteger.valueOf(now.getEpochSecond())
                .multiply(BigInteger.valueOf(1000));
        millis = millis.add(BigInteger.valueOf(now.getNano())
                .divide(BigInteger.valueOf(1_000_000)));
        return new Date(millis.longValue());
    }
    
    /**
     * Converts the specified {@link java.util.Date Date} from the Java
     * Util package to a {@link java.time.LocalDateTime LocalDateTime} 
     * from the Java Time package.
     * 
     * @param date2Convert the {@code java.util.Date} to convert to a 
     * {@code java.time.LocalDateTime} instance
     * @return the specified {@code Date} as a {@code LocalDateTime} instance
     */
    public static LocalDateTime convertDate2LocalDateTime(Date date2Convert) {
        return date2Convert.toInstant().atZone(ZoneId.systemDefault())
                .toLocalDateTime();
    }
    
    /**
     * Converts the specified {@link java.time.LocalDateTime LocalDateTime} 
     * from the Java Time package to a {@link java.util.Date Date} from
     * the Java Util package.
     * 
     * @param date2Convert the {@code java.time.LocalDateTime} to convert to a 
     * {@code java.util.Date} instance
     * @return the specified {@code LocalDateTime} as a {@code Date} instance
     */
    public static Date convertLocalDateTime2Date(LocalDateTime date2Convert) {
        return Date.from(date2Convert.atZone(ZoneId.systemDefault()).toInstant());
    }

}
