#!/bin/bash

if [ "$#" -ne 1 ]; then
    echo ""
    echo "usage: git-push.sh <NEW_BRANCH_NAME>"
    echo ""
    
    exit 1
fi

git checkout -b $1
git add .
git commit


git push --set-upstream origin -o merge_request.merge_when_pipeline_succeeds $1

echo ""
echo "Your changes to the project have now been pushed to the remote repository."
echo "You now need to go to the Merge requests page on GitLab and create your"
echo "    merge request to get the changes approved and merged into the codebase."
echo ""
echo "By using this script to push your changes, the merge request will be set"
echo "to automatically merge when all pipelines successfully complete. This"
echo "feature saves us all a step in the merge process."
echo ""
echo "Once your changes have been successfully merged to the codebase, you need "
echo "to run git-reset <BRANCH>. Typically, <BRANCH> is HEAD to get the newest "
echo "revision of the entire codebase."
echo ""
echo "Thank you for using the git-push.sh script."
echo ""

exit 0
